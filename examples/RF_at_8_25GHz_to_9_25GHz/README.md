# Example 8.25-9.25GHz RF

This example configures a CR4 on the CR8 to have all 4 channels tuned to downconverting 8.25GHz-9.25GHz to 2.61GHz-1.61GHz (IF Flipped) respectively.

## Setup 

Supplies needed

+ A signal generator capable of providing 8.25GHz to 9.25GHz

+ A 4 way splitter capable of splitting up to 9.25GHz

+ An oscilloscope to observe channel-channel phase synchronization or a spectrum analyzer to observe IF output.

+ 9 SMA cables

Instructions

1. Connect the Signal generator to the 4 way splitter using an SMA cable

2. Connect the 4 outputs of the splitter to RF Channels 1-4 of the CR8 using an SMA cable

3. Connect the 4 outputs of the CR8, IF Channels 1-4, to the oscilloscope or 1 output to the spectrum analyzer using an SMA cable

4. Set the oscilloscope sample at least at 4 Gsps to observe the 1.61GHz-2.61GHz output. If using a spectrum analyzer, set the center frequency to 2.11GHz and a span of 1GHz.

5. Connect the Power and USB cable to the CR8

6. Power on the device

## Usage

Windows

1. Power-on and connect the CR8 to the host computer

2. Double-click/run the RUNME.bat file

3. Observe the IF output at 2.11GHz.

4. Sweep the RF frequency from 8.25GHz to 9.25GHz
